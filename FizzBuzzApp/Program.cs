﻿using System;

namespace FizzBuzzApp
{
    class Program
    {
        static void Main(string[] args)
        {
            FizzBuzzService service = new FizzBuzzService();
            for (int i = 1; i <= 100; i++)
            {
                Console.WriteLine(i + " : " + service.FizzBuzz(i));
            }
        }
    }
}
