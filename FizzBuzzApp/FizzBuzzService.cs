﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzzApp
{
    public class FizzBuzzService
    {
        public String FizzBuzz(int input)
        {
            Boolean fizz = input % 3 == 0;
            Boolean buzz = input % 5 == 0;

            if (fizz && buzz)
            {
                return "FizzBuzz";
            }
            else if (fizz)
            {
                return "Fizz";
            }
            else if (buzz)
            {
                return "Buzz";
            }
            return input.ToString();
        }
    }
}
