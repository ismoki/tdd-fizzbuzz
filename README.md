# TDD Kata: FizzBuzz in C#

This repository contains the reference solution for FizzBuzz kata with C#. 

The objective of this exercise was to write the following program following the TDD principles:

 - Write a program that prints one line for each number from 1 to 100
 - For multiples of three print Fizz instead of the number
 - For the multiples of five print Buzz instead of the number
 - For numbers which are multiples of both three and five print FizzBuzz instead of the
   number

For more TDD katas visit https://kata-log.rocks/tdd


