﻿using NUnit.Framework;

namespace FizzBuzzApp.Tests
{
    class FizzBuzzParameterizedTests
    {
        private FizzBuzzService service;

        [SetUp]
        public void setup()
        {
            service = new FizzBuzzService();
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(4)]
        [TestCase(7)]
        [TestCase(8)]
        [TestCase(11)]
        public void ShouldReturnInput(int input)
        {
            Assert.AreEqual(input.ToString(), service.FizzBuzz(input));
        }

        [TestCase(3)]
        [TestCase(6)]
        [TestCase(9)]
        [TestCase(12)]
        [TestCase(18)]
        [TestCase(21)]
        public void ShouldReturnFizz(int input)
        {
            Assert.AreEqual("Fizz", service.FizzBuzz(input));
        }

        [TestCase(5)]
        [TestCase(10)]
        [TestCase(20)]
        [TestCase(25)]
        [TestCase(35)]
        [TestCase(40)]
        public void ShouldReturnBuzz(int input)
        {
            Assert.AreEqual("Buzz", service.FizzBuzz(input));
        }
    }
}
