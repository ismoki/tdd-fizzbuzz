using NUnit.Framework;

namespace FizzBuzzApp.Tests
{
    public class Tests
    {
        private FizzBuzzService service;

        [SetUp]
        public void Setup()
        {
            service = new FizzBuzzService();
        }

        [Test]
        public void ShouldReturnInput_WhenInput1()
        {
            int input = 1;
            Assert.AreEqual("1", service.FizzBuzz(input));
        }

        [Test]
        public void ShouldReturnInput_WhenInput2()
        {
            int input = 2;
            Assert.AreEqual("2", service.FizzBuzz(input));
        }

        [Test]
        public void ShouldReturnFizz_WhenInput3()
        {
            int input = 3;
            Assert.AreEqual("Fizz", service.FizzBuzz(input));
        }

        [Test]
        public void ShouldReturnInput_WhenInput4()
        {
            int input = 4;
            Assert.AreEqual("4", service.FizzBuzz(input));
        }

        [Test]
        public void ShouldReturnBuzz_WhenInput5()
        {
            int input = 5;
            Assert.AreEqual("Buzz", service.FizzBuzz(input));
        }

        [Test]
        public void ShouldReturnFizz_WhenInput6()
        {
            int input = 6;
            Assert.AreEqual("Fizz", service.FizzBuzz(input));
        }

        [Test]
        public void ShouldReturnInput_WhenInput7()
        {
            int input = 7;
            Assert.AreEqual("7", service.FizzBuzz(input));
        }

        [Test]
        public void ShouldReturnInput_WhenInput8()
        {
            int input = 8;
            Assert.AreEqual("8", service.FizzBuzz(input));
        }

        [Test]
        public void ShouldReturnFizzBuzzWhenInput15()
        {
            int input = 15;
            Assert.AreEqual("FizzBuzz", service.FizzBuzz(input));
        }
    }
}